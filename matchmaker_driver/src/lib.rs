use base64;
use futures::future::lazy;
use futures::sync::oneshot;
use futures::{Future, Stream};
use hyper::Client;
use hyper::{header::HeaderValue, Body, Method, Request};
use hyper_tls::HttpsConnector;
use lazy_static::lazy_static;
use log::{log, trace};
use secp256k1;
use serde::Serialize as SerdeSerialize;
use serde_derive::{Deserialize, Serialize};
use serde_yaml;
use std::collections::HashMap;
use std::fmt::Display;
use std::fs::File;
use std::io;
use std::path::Path;
use std::sync::{Arc, Mutex};
use tokio::runtime::Runtime;
use uuid::Uuid;

lazy_static! {
    static ref SECP256K1: secp256k1::Secp256k1 =
        secp256k1::Secp256k1::with_caps(secp256k1::ContextFlag::SignOnly);
}

#[cfg(feature = "debug")]
const CREDENTIALS: &str = ".quup/debug/credentials.yaml";
#[cfg(not(feature = "debug"))]
const CREDENTIALS: &str = ".quup/credentials.yaml";

#[cfg(feature = "debug")]
const SITE: &str = "http://localhost:8080";
#[cfg(not(feature = "debug"))]
const SITE: &str = "https://quati.systems";

#[derive(Debug)]
pub enum Error {
    Base64(base64::DecodeError),
    Crypto(secp256k1::Error),
    Http(hyper::Error),
    Json(serde_json::Error),
    AtCreation(String, String),
    AtAuthentication(String, String),
}

fn urlsafe_b64decode(s: &str) -> Result<Vec<u8>, Error> {
    base64::decode(&s.replace("-", "+").replace("_", "/")).map_err(Error::Base64)
}

fn urlsafe_b64encode(s: &[u8]) -> String {
    base64::encode(s).replace("+", "-").replace("/", "_")
}

#[derive(Debug, Clone, Deserialize)]
pub struct Credentials {
    account_uuid: String,
    email: String,
    full_name: String,
    token: String,
}

impl Credentials {
    pub fn load<P: AsRef<Path> + Display>(path: P) -> Result<Self, io::Error> {
        let error = |msg| io::Error::new(io::ErrorKind::Other, msg);
        let cred_path = format!("{}/{}", dirs::home_dir().unwrap().to_str().unwrap(), path);
        println!("{:?}", cred_path);
        let deserialized: HashMap<String, Credentials> =
            serde_yaml::from_reader(File::open(cred_path)?)
                .map_err(|e| error(format!("Credentials deserialization failed: {:?}", e)))?;

        Ok(deserialized
            .get("quati")
            .ok_or_else(|| error("Mandatory key `quati` not found.".to_owned()))?
            .clone())
    }

    fn sign(&self, hash: &str) -> Result<String, Error> {
        let token = urlsafe_b64decode(&self.token)?;
        let hash = urlsafe_b64decode(hash)?;
        let secret_key =
            secp256k1::SecretKey::from_slice(&SECP256K1, &token[..]).map_err(Error::Crypto)?;
        let message = secp256k1::Message::from_slice(&hash[..]).map_err(Error::Crypto)?;
        let signature = SECP256K1
            .sign(&message, &secret_key)
            .map_err(Error::Crypto)?;

        Ok(urlsafe_b64encode(
            &signature.serialize_compact(&SECP256K1)[..],
        ))
    }
}

#[derive(Debug, Deserialize)]
#[serde(tag = "status", content = "response")]
enum Response<T> {
    Ok(T),
    Err { typ: String, message: String },
}

#[derive(Debug, Deserialize)]
struct DemandResponse {
    demand_uuid: String,
    nonce_hash: String,
}

#[derive(Debug, Serialize)]
struct Authentication {
    signature: String,
}

/// Panics on error (and this is the intended behavior).
fn jsonify<S: SerdeSerialize>(uri: &str, x: &S) -> Request<Body> {
    let json = serde_json::to_string(x).unwrap();
    let mut req = Request::new(Body::from(json));
    *req.method_mut() = Method::POST;
    *req.uri_mut() = uri.parse().unwrap();
    req.headers_mut().insert(
        "content-type",
        HeaderValue::from_str("application/json").unwrap(),
    );

    req
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Demand {
    server_uuid: Uuid,
    u3id: Uuid,
    quantity: usize,
}

impl Demand {
    pub fn new(server_uuid: Uuid, u3id: Uuid, quantity: usize) -> Demand {
        Demand {
            server_uuid,
            u3id,
            quantity,
        }
    }

    pub fn post(
        &self,
        credentials: Arc<Credentials>,
        runtime: &mut Runtime,
    ) -> impl Future<Item = (), Error = Error> {
        let (confirmation_sender, confirmation_receiver) = oneshot::channel();
        let post_demand_request = jsonify(&(SITE.to_owned() + "/demand"), &self);

        runtime.spawn(lazy(move || {
            #[cfg(feature = "debug")]
            let client = Client::new();

            #[cfg(not(feature = "debug"))]
            let client = {
                let https = HttpsConnector::new(4).unwrap();
                Client::builder().build::<_, hyper::Body>(https)
            };

            let do_post = move |request| {
                client
                    .request(request)
                    .and_then(|res| {
                        trace!("Response for POST: {}", res.status());
                        res.into_body()
                            .map(|chunk| String::from_utf8_lossy(&*chunk).to_string())
                            .fold(String::new(), |mut acc, chunk| {
                                acc += &chunk;
                                Ok(acc) as Result<String, hyper::Error>
                            })
                    }).map_err(Error::Http)
            };

            do_post(post_demand_request)
                .and_then(|response_text| {
                    println!("{:?}", response_text);
                    let demand_response =
                        serde_json::from_str::<Response<DemandResponse>>(&response_text)
                            .map_err(Error::Json)?;

                    match demand_response {
                        Response::Ok(demand_response) => Ok(demand_response),
                        Response::Err { typ, message } => Err(Error::AtCreation(typ, message)),
                    }
                }).and_then(move |demand_response| {
                    let signature = credentials.sign(&demand_response.nonce_hash)?;
                    Ok((demand_response, signature))
                }).and_then(move |(demand_response, signature)| {
                    let uri = format!("{}/demand/{}/auth", SITE, demand_response.demand_uuid);

                    do_post(jsonify(&uri, &Authentication { signature }))
                }).and_then(|response_text| {
                    println!("{:?}", response_text);
                    let confirmation =
                        serde_json::from_str::<Response<serde_json::Value>>(&response_text)
                            .map_err(Error::Json)?;

                    match confirmation {
                        Response::Ok(_) => {
                            trace!("Demand authenticated");
                            Ok(())
                        }
                        Response::Err { typ, message } => {
                            Err(Error::AtAuthentication(typ, message))
                        }
                    }
                }).then(|status| {
                    confirmation_sender.send(status).ok();
                    Ok(())
                })
        }));

        confirmation_receiver
            .map_err(|_| unreachable!())
            .and_then(|status| status)
    }
}

pub struct QuatiClient {
    runtime: Arc<Mutex<Runtime>>,
    credentials: Arc<Credentials>,
}

impl QuatiClient {
    pub fn from_installed() -> Result<Self, io::Error> {
        Ok(QuatiClient {
            runtime: Arc::new(Mutex::new(Runtime::new().unwrap())),
            credentials: Arc::new(Credentials::load(CREDENTIALS)?),
        })
    }

    pub fn demand(&self, demand: &Demand) -> Result<(), Error> {
        demand
            .post(self.credentials.clone(), &mut self.runtime.lock().unwrap())
            .wait()
    }
}

// #[test]
// fn authentication_sample() {
//     let credentials = Credentials::load(CREDENTIALS).unwrap();
//     let runtime = Arc::new(Mutex::new(Runtime::new().unwrap()));
//
//     let demand = if cfg!(feature = "debug") {
//         println!("Debug",);
//         Demand::new(
//             "21c6ecc0-9474-4e5a-9b5f-af07c150b8dd".parse().unwrap(),
//             "d2bf8975-00df-45fc-9055-4f92fb9c6be3".parse().unwrap(),
//             3,
//         )
//     } else {
//         Demand::new(
//             "0755cd05-8f83-49de-a21f-dcb494dae3d8".parse().unwrap(),
//             "5851b182-11a4-4984-a833-787ccddc7a3f".parse().unwrap(),
//             3,
//         )
//     };
//
//     runtime.lock().unwrap().spawn(lazy(move || {
//         demand.post(Arc::new(credentials), &mut runtime).then(|status| {
//             println!("{:#?}", status);
//             Ok(())
//         })
//     }));
//
//     runtime.lock().un
// }

#[test]
fn client() {
    let client = QuatiClient::from_installed().unwrap();

    let demand = if cfg!(feature = "debug") {
        println!("Debug",);
        Demand::new(
            "21c6ecc0-9474-4e5a-9b5f-af07c150b8dd".parse().unwrap(),
            "d2bf8975-00df-45fc-9055-4f92fb9c6be3".parse().unwrap(),
            3,
        )
    } else {
        Demand::new(
            "0755cd05-8f83-49de-a21f-dcb494dae3d8".parse().unwrap(),
            "5851b182-11a4-4984-a833-787ccddc7a3f".parse().unwrap(),
            3,
        )
    };

    println!("{:#?}", client.demand(demand));
}
