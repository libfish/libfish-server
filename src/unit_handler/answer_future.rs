use futures::channel::oneshot;
use futures::task::{LocalWaker, Poll};
use futures::{ready, Future};
use std::fmt::Debug;
use std::pin::Pin;

use super::Error;

pub struct AnswerFuture<A: Debug> {
    receive_back: oneshot::Receiver<Result<A, Error>>,
    oid: u64, // TODO what about this? Only need for 'em logs...
}

impl<A: Debug> AnswerFuture<A> {
    pub(crate) fn new(
        receive_back: oneshot::Receiver<Result<A, Error>>,
        oid: u64,
    ) -> AnswerFuture<A> {
        AnswerFuture { receive_back, oid }
    }
}

impl<A: Debug> Future for AnswerFuture<A> {
    type Output = Result<A, Error>;

    fn poll(mut self: Pin<&mut Self>, lw: &LocalWaker) -> Poll<Result<A, Error>> {
        let pinned = Pin::new(&mut self.receive_back);
        let answer = ready!(pinned.poll(lw)).map_err(|_| Error::SendBackCanceled);

        log::debug!("Receiving oid={:x}", self.oid);

        Poll::Ready(
            answer
                .and_then(|answer| {
                    log::trace!("answer: {:#?}", answer);
                    answer
                })
                .map_err(|_| {
                    log::warn!("this answer was canceled");
                    Error::SendBackCanceled
                }),
        )
    }
}
