use futures::lock::Mutex;
use rand::rngs::SmallRng;
use rand::FromEntropy;
use rand::Rng;
use std::collections::BTreeMap;
use std::sync::Arc;

#[derive(Debug)]
pub struct Register<T> {
    rng: Arc<Mutex<SmallRng>>,
    register: Arc<Mutex<Option<BTreeMap<u64, T>>>>,
}

impl<T> Clone for Register<T> {
    fn clone(&self) -> Self {
        Register {
            rng: self.rng.clone(),
            register: self.register.clone(),
        }
    }
}

impl<T> Default for Register<T> {
    fn default() -> Register<T> {
        Register::new()
    }
}

impl<T> Register<T> {
    pub fn new() -> Register<T> {
        Register {
            register: Arc::new(Mutex::new(Some(BTreeMap::new()))),
            rng: Arc::new(Mutex::new(SmallRng::from_entropy())),
        }
    }

    pub async fn insert(&self, value: T) -> Option<u64> {
        // Lock mutexes
        let mut register = await!(self.register.lock());
        let mut rng = await!(self.rng.lock());

        // Insert in dict:
        let oid = rng.gen();
        register.as_mut()?.insert(oid, value);

        Some(oid)
    }

    pub async fn remove(&self, oid: u64) -> Option<T> {
        let mut register = await!(self.register.lock());
        register.as_mut()?.remove(&oid)
    }

    pub async fn drain(&self) -> Option<impl Iterator<Item = T>> {
        let mut register = await!(self.register.lock());
        let register = register.take()?;

        Some(register.into_iter().map(|(_, value)| value))
    }
}
