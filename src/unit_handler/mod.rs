mod answer_future;
mod register;

use fish_core::message::{ExecType, FromClient, FromServer};
use fish_core::Unit;
use futures::channel::oneshot;
use futures::future::RemoteHandle;
use futures::task::{Spawn, SpawnExt};
use futures::StreamExt;

use crate::ws::allocation::{AllocationReceiveHalf, AllocationSendHalf};
use crate::ws::factory::HandlerAllocator;
use crate::ws::matching::Priority;
use crate::ws::socket_event;

use self::answer_future::AnswerFuture;
use self::register::Register;

#[derive(Debug)]
pub enum Error {
    WebSocket(ws::Error),
    StreamDried,
    SocketEvent(socket_event::Error),
    UnexpectedMessage(String),
    UnitLost,
    QuestionCanceled,
    SendBackCanceled,
    DecodeError(bincode::Error),
}

pub struct UnitHandler<U: Unit> {
    receiver_task: Option<RemoteHandle<Result<(U, Option<U::State>), Error>>>,
    send_half: AllocationSendHalf,
    register: Register<oneshot::Sender<Result<U::Answer, Error>>>,
}

impl<U: Unit> UnitHandler<U> {
    /// Initiates a new unit handler.
    pub(crate) async fn init<S: 'static + Spawn>(
        unit: U,
        mut allocator: HandlerAllocator,
        mut executor: S,
    ) -> Result<UnitHandler<U>, Error> {
        let register = Register::new();
        let (send_half, receive_half) = await!(allocator.allocate(Priority::now()));

        log::debug!("UnitHandler allocated");

        // Send message to allocation:
        send_half
            .send(&FromServer::Unit {
                unit,
                maybe_state: None,
                exec_type: ExecType::Retrieving,
            })
            .map_err(|err| Error::WebSocket(err))?;

        log::debug!("Unit sent");

        let receiver_task = Some(
            executor
                .spawn_with_handle(ReceiverTask(receive_half, register.clone()).run())
                .unwrap(),
        );

        log::debug!("Receiver task spawned");

        Ok(UnitHandler {
            receiver_task,
            send_half,
            register,
        })
    }

    /// Asks a question ad returns a future for its answer.
    pub async fn ask(&self, question: U::Question) -> Result<AnswerFuture<U::Answer>, Error> {
        // create channel
        let (send_back, receive_back) = oneshot::channel();

        // register oid
        let oid = await!(self.register.insert(send_back)).ok_or_else(|| Error::QuestionCanceled)?;

        log::debug!("Asking oid=0x{:x}", oid);
        log::trace!("question: {:#?}", question);

        // send message
        self.send_half
            .send::<U>(&FromServer::Question { oid, question })
            .map_err(|err| {
                log::error!("Error while sending question: {:#?}", err);
                Error::WebSocket(err)
            })?;

        log::debug!("Done sending. Now waiting for response");

        // receive message in the future
        Ok(AnswerFuture::new(receive_back, oid))
    }

    /// Asks the unit to stop runing and to return its state.
    pub fn shutdown(&self) -> Result<(), Error> {
        log::debug!("Sending shutdown message");
        self.send_half
            .send::<U>(&FromServer::Retrieve)
            .map_err(|err| Error::WebSocket(err))
    }

    /// Evaluates when the unit is finished running.
    #[must_use = "Dropping this causes the task to abort"]
    pub async fn retrieve(&mut self) -> Result<(U, Option<U::State>), Error> {
        log::debug!("Started waiting on retrieve");
        await!(self.receiver_task.take().expect("Can only retrieve once"))
    }

    /// Asks the unit to shut down and waits unit the while process is complete.
    #[must_use = "Dropping this causes the task to abort"]
    pub async fn retrieve_now(&mut self) -> Result<(U, Option<U::State>), Error> {
        self.shutdown()?;
        await!(self.retrieve())
    }
}

impl<U: Unit> Drop for UnitHandler<U> {
    fn drop(&mut self) {
        log::debug!("Dropping unit handler");

        if let Some(task) = self.receiver_task.take() {
            // Drop without cancelling task.
            task.forget();

            // Send the message to the unit to forget the wole thing.
            self.send_half
                .send::<U>(&FromServer::End)
                .map_err(|err| Error::WebSocket(err))
                .ok();
        }
    }
}

struct ReceiverTask<U: Unit>(
    AllocationReceiveHalf,
    Register<oneshot::Sender<Result<U::Answer, Error>>>,
);

impl<U: Unit> ReceiverTask<U> {
    async fn run(self) -> Result<(U, Option<U::State>), Error> {
        log::debug!("Receiver task started");

        // Dismember struct:
        let ReceiverTask(mut receive_half, register) = self;

        // Wait for response:
        let response = await!(receive_half.event_stream_mut().next())
            .ok_or_else(|| Error::StreamDried)?
            .decode::<U>()
            .map_err(|err| Error::SocketEvent(err))?;

        // Match response:
        match response {
            FromClient::Loaded => log::debug!("Unit loaded"),
            msg => {
                log::error!("Unexpected message at load: {:#?}", msg);
                return Err(Error::UnexpectedMessage(format!("{:?}", msg)));
            }
        }

        // Ok, now you can start sending messages.
        while let Some(response) = await!(receive_half.event_stream_mut().next()) {
            let response = response
                .decode()
                .map(|response| {
                    log::trace!("Got response: {:#?}", response);
                    response
                })
                .map_err(|err| {
                    log::error!("Got error: {:#?}", err);
                    Error::SocketEvent(err)
                })?;

            match response {
                // Got an answer! Send it to whoever was waiting for it.
                FromClient::Answer { oid, answer } => {
                    if let Some(send_back) = await!(register.remove(oid)) {
                        send_back.send(Ok(answer)).ok();
                    };
                }
                // Time to pack your things.
                FromClient::Retrieve {
                    maybe_unit,
                    maybe_state,
                } => {
                    log::debug!("Received retrieve request");

                    // Drain register and tell whoever is waiting:
                    if let Some(drain) = await!(register.drain()) {
                        for send_back in drain {
                            send_back.send(Err(Error::QuestionCanceled)).ok();
                        }
                    }

                    // Deserialize unit:
                    let unit = maybe_unit.ok_or_else(|| {
                        log::error!("Lost unit");
                        Error::UnitLost
                    })?;

                    log::trace!("Retrieved");

                    // Deserialize state:
                    return Ok((unit, maybe_state));
                }
                // Everything else is an error.
                msg => {
                    log::error!("Unexpected message: {:#?}", msg);
                    return Err(Error::UnexpectedMessage(format!("{:?}", msg)));
                }
            }
        }

        // If you've got here, it's an error.
        log::error!("Socket event stream dried.");
        Err(Error::StreamDried)
    }
}
