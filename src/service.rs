use fish_core::Unit;
use futures::channel::oneshot;
use futures::executor::block_on;
use futures::stream::{Stream, StreamExt};
use futures::task::{Spawn, SpawnExt};
use futures::Future;
use std::any::Any;
use std::net::ToSocketAddrs;
use std::thread;

use crate::broker::{Broker, Strategy};
// use crate::controller::{self, Controller};
use crate::unit_handler::{self, UnitHandler};
use crate::ws::factory::{handler_channel, HandlerAllocator};

pub enum ServiceError {
    /// Internal error when running WebSocket.
    WebSocket(ws::Error),
    /// WebSocket did not spawn and the thread ended early.
    SpawnFailed,
    /// WebSocket thread panicked.
    ThreadPanicked(Box<Any + Send + 'static>),
    /// WebSocket creation failed (returned at shutdown)
    WebSocketCreationFailed,
    /// Spawning the service task failed.
    SpawnError,
}

/// Spawns new units in the network.
pub struct UnitService<Sp: Spawn> {
    /// Spawner of futures.
    spawn: Sp,
    /// Handle to the thread where the WebSocket is running.
    thread_handle: Option<thread::JoinHandle<Option<Result<(), ws::Error>>>>,
    /// A bradcasting sender to broadcast a shutdown singal to the WebSocket.
    broadcaster: ws::Sender,
    /// A factory of new allocations.
    allocator: HandlerAllocator,
}

impl<Sp: 'static + Spawn + Clone + Send> UnitService<Sp> {
    /// Creates a WebSocket listening to some address `addr`. This function blocks and should olny
    /// be used as a convenience in a synchronous environment. See `listen` for the asynchronous
    /// version.
    pub fn new<A: 'static + Send + ToSocketAddrs, S: Strategy>(
        addr: A,
        strategy: S,
        mut spawn: Sp,
    ) -> Result<UnitService<Sp>, ServiceError> {
        block_on(
            spawn
                .spawn_with_handle(Self::listen(addr, strategy, spawn.clone()))
                .map_err(|_| ServiceError::SpawnError)?,
        )
    }

    /// Creates a service running a WebSocket listening to some address `addr`.
    pub async fn listen<A: 'static + Send + ToSocketAddrs, S: Strategy>(
        addr: A,
        strategy: S,
        mut spawn: Sp,
    ) -> Result<UnitService<Sp>, ServiceError> {
        let broker = await!(Broker::spawn(strategy));
        let (factory, allocator) = await!(handler_channel(broker, &mut spawn));
        let (broadcaster_sender, broadcaster_receiver) = oneshot::channel();

        let thread_handle = thread::spawn(move || {
            // Configure WebSocket:
            let mut settings = ws::Settings::default();
            settings.tcp_nodelay = true;

            // Socket is not `Send` with permessage-deflate.
            // That is why this is so convoluted.
            match ws::Builder::new().with_settings(settings).build(factory) {
                Ok(socket) => {
                    broadcaster_sender.send(Ok(socket.broadcaster())).ok();
                    Some(socket.listen(addr).map(|_socket| ()))
                }
                Err(error) => {
                    broadcaster_sender.send(Err(error)).ok();
                    None
                }
            }
        });

        let broadcaster = await!(broadcaster_receiver)
            .map_err(|_| ServiceError::SpawnFailed)?
            .map_err(ServiceError::WebSocket)?;

        Ok(UnitService {
            spawn,
            broadcaster,
            thread_handle: Some(thread_handle),
            allocator,
        })
    }

    /// Sends shutdown signal to the underlying WebSocket and waits for it to terminate, returning
    /// an error if the execution was no successful. This function works on thread level and has no
    /// asynchronous version. It is safe to call it from an asynchronous context as well.
    pub fn shutdown(&mut self) -> Result<(), ServiceError> {
        // Take ownership of handle.
        let handle = self.thread_handle.take().unwrap(); // safe.

        // Shutdown the WebSocket:
        self.broadcaster
            .shutdown()
            .map_err(ServiceError::WebSocket)?;

        // Analyze handle:
        handle
            .join()
            .map_err(ServiceError::ThreadPanicked)?
            .ok_or(ServiceError::WebSocketCreationFailed)?
            .map_err(ServiceError::WebSocket)
    }

    /// Creates one new unit in the network.
    #[inline(always)]
    #[must_use]
    pub fn spawn<U: Unit>(
        &self,
        unit: U,
    ) -> impl Future<Output = Result<UnitHandler<U>, unit_handler::Error>> + Send {
        UnitHandler::init(unit, self.allocator.clone(), self.spawn.clone())
    }

    // Creates a never-ending stream of units, at the rate they start appearing.
    #[inline(always)]
    #[must_use]
    pub fn stream<U, S>(
        &self,
        stream: S,
    ) -> impl Stream<Item = Result<UnitHandler<U>, unit_handler::Error>> + Send
    where
        U: Unit,
        S: Stream<Item = U> + Send,
    {
        let allocator = self.allocator.clone();
        let spawn = self.spawn.clone();
        stream.then(move |unit| UnitHandler::init(unit, allocator.clone(), spawn.clone()))
    }
}

impl<Sp: Spawn> Drop for UnitService<Sp> {
    fn drop(&mut self) {
        // Shutdown the WebSocket:
        self.broadcaster.shutdown().ok();

        // Wait for the thread running the WebSocket to finish:
        self.thread_handle.take().map(|handle| handle.join().ok());
    }
}
