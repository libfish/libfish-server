use futures::channel::mpsc;
use futures::executor::block_on;
use futures::stream::StreamExt;
use matchmaker_driver::QuatiClient;
use std::thread;

// JavaScripty
#[allow(unused)]
pub trait Strategy: 'static + Send {
    fn on_init(&mut self, client: &QuatiClient) {}
    fn on_received(&mut self, client: &QuatiClient) {}
    fn on_discarded(&mut self, client: &QuatiClient) {}
    fn on_shutdown(&mut self, client: &QuatiClient) {}
}

/// Ths strategy does nothing. It represents external control, without any input from the
/// application. Maybe this fits your needs.
pub struct External;

impl Strategy for External {}

#[derive(Debug)]
pub(crate) enum BrokerEvent {
    Received, // TODO Received(Uuid)
    Discarded,
    Shutdown,
}

#[derive(Clone, Debug)]
pub(crate) struct Broker {
    event_sender: mpsc::UnboundedSender<BrokerEvent>,
}

impl Broker {
    pub(crate) async fn spawn<S: Strategy>(mut strategy: S) -> Self {
        let (event_sender, mut event_receiver) = mpsc::unbounded();
        let client = QuatiClient::from_installed().unwrap();

        thread::spawn(move || {
            strategy.on_init(&client);

            loop {
                match block_on(event_receiver.next()) {
                    Some(BrokerEvent::Received) => strategy.on_received(&client),
                    Some(BrokerEvent::Discarded) => strategy.on_discarded(&client),
                    Some(BrokerEvent::Shutdown) | None => {
                        strategy.on_shutdown(&client);
                        break;
                    }
                }
            }
        });

        Broker { event_sender }
    }

    pub(crate) fn send_event(&self, event: BrokerEvent) {
        self.event_sender.unbounded_send(event).ok();
    }
}
