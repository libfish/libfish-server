#![feature(async_await, await_macro, futures_api, pin, arbitrary_self_types)]

pub mod broker;
// pub mod controller;
pub mod service;
pub mod unit_handler;
mod ws;

// pub use self::controller::Controller;
pub use self::service::UnitService;
pub use self::unit_handler::UnitHandler;

// TODO:
// Too much copying around. (Don't take ownership, use "prototypes")
// Two steps of serialization (more copying around): depends on
// Implement more fine grain socket config.
// Scurity issues. In special: can I be fooled into waiting forever? (DoS)
// Make a mock.

// use futures::executor::{block_on, ThreadPool};
// use futures::future::ready;
// use futures::task::SpawnExt;
//
// async fn foo() -> usize {
//     await!(ready(()));
//     1
// }
//
// #[allow(unused)]
// fn main() {
//     let mut thread_pool = ThreadPool::new().unwrap();
//     let receivers = (0..4)
//         .map(|_| thread_pool.spawn_with_handle(foo()).unwrap())
//         .collect::<Vec<_>>();
//
//     for receiver in receivers {
//         println!("{}", block_on(receiver));
//     }
// }
