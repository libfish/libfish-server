use futures::channel::mpsc::{self, UnboundedSender};
use futures::channel::oneshot;
use futures::task::{Spawn, SpawnExt};
use futures::{Future, FutureExt};
use ws::Factory;

use super::allocation::{Allocation, AllocationReceiveHalf, AllocationSendHalf};
use super::matching::{Matcher, Priority};
use super::message_handler::MessageHandler;
use crate::broker::{Broker, BrokerEvent};

/// A factory of `MessageHandler` that is called whena new connection s established.
pub struct HandlerFactory {
    /// The MPMC channel which produces new `Allocation`s to be consumed by a `HandlerAllocator`.
    allocation_sender: UnboundedSender<Allocation>,
    /// A broker to signal when new conncetions appear and when an allocation is dropped.
    broker: Broker,
}

/// A structure that issues futures to new (or recycled) allocations.
#[derive(Clone)]
pub struct HandlerAllocator {
    request_sender: UnboundedSender<(Priority, oneshot::Sender<Allocation>)>,
    /// The MPMC channel which is the source of new allocations.
    allocation_sender: UnboundedSender<Allocation>,
}

/// Produces a `HandlerFactory` and a `HandlerAllocator` conected to eachother.
pub(crate) async fn handler_channel<Sp: Spawn>(
    broker: Broker,
    spawn: &mut Sp,
) -> (HandlerFactory, HandlerAllocator) {
    // Create channels:
    let (request_sender, request_receiver) = mpsc::unbounded();
    let (allocation_sender, allocation_receiver) = mpsc::unbounded();

    // Matching loop:
    spawn
        .spawn(Matcher::new(request_receiver, allocation_receiver).run())
        .unwrap();

    // Create structures:
    (
        HandlerFactory {
            allocation_sender: allocation_sender.clone(),
            broker,
        },
        HandlerAllocator {
            allocation_sender,
            request_sender,
        },
    )
}

impl Factory for HandlerFactory {
    // Use per-message ws::deflate extension (maybe good?):
    type Handler = ws::deflate::DeflateHandler<MessageHandler>;

    fn connection_made(&mut self, ws_sender: ws::Sender) -> Self::Handler {
        // Tell the broker:
        self.broker.send_event(BrokerEvent::Received);

        // Create message handler and allocation:
        let (event_sender, event_stream) = mpsc::unbounded();
        let handler = MessageHandler::new(event_sender);
        let send_half = AllocationSendHalf::new(ws_sender, self.broker.clone());
        let receive_half = AllocationReceiveHalf::new(event_stream);

        // Send allocation to fins its match:
        self.allocation_sender
            .unbounded_send((send_half, receive_half))
            .ok();

        // Return handler to socket (with per-message delfate extension):
        ws::deflate::DeflateHandler::new(handler)
    }

    fn on_shutdown(&mut self) {
        self.broker.send_event(BrokerEvent::Shutdown);
    }
}

impl HandlerAllocator {
    /// Create a future which resolves to an `Allocation`.
    pub fn allocate(
        &mut self,
        priority: Priority,
    ) -> impl Future<Output = (AllocationSendHalf, AllocationReceiveHalf)> {
        let (sender, receiver) = oneshot::channel();
        self.request_sender.unbounded_send((priority, sender)).ok();

        receiver.map(Result::unwrap) // TODO by now...
    }
}
