use binary_heap_plus::BinaryHeap;
use futures::channel::{mpsc, oneshot};
use futures::StreamExt;
use std::cmp::Ordering;
use std::collections::VecDeque;
use std::time::Instant;

use super::allocation::Allocation;

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum Priority {
    NewAt(Instant),
    IdleSince(Instant),
}

impl Priority {
    pub fn new() -> Self {
        Priority::NewAt(Instant::now())
    }

    pub fn now() -> Self {
        Priority::IdleSince(Instant::now())
    }
}

impl Default for Priority {
    fn default() -> Self {
        Self::new()
    }
}

impl Ord for Priority {
    fn cmp(&self, other: &Self) -> Ordering {
        match (&self, other) {
            (Priority::NewAt(t_self), Priority::NewAt(t_other)) => t_self.cmp(&t_other),
            (Priority::NewAt(_), Priority::IdleSince(_)) => Ordering::Less,
            (Priority::IdleSince(_), Priority::NewAt(_)) => Ordering::Greater,
            (Priority::IdleSince(t_self), Priority::IdleSince(t_other)) => t_self.cmp(&t_other),
        }
    }
}

impl PartialOrd for Priority {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// A half of a match. A match happens when we have an allocation and a requests paired.
enum MatchHalf {
    Supply(Allocation),
    Demand(Priority, oneshot::Sender<Allocation>),
}

pub(super) struct Matcher {
    request_receiver: mpsc::UnboundedReceiver<(Priority, oneshot::Sender<Allocation>)>,
    allocation_receiver: mpsc::UnboundedReceiver<Allocation>,
}

impl Matcher {
    pub fn new(
        request_receiver: mpsc::UnboundedReceiver<(Priority, oneshot::Sender<Allocation>)>,
        allocation_receiver: mpsc::UnboundedReceiver<Allocation>,
    ) -> Self {
        Matcher {
            request_receiver,
            allocation_receiver,
        }
    }

    pub async fn run(self) {
        let mut request_heap =
            BinaryHeap::new_by_key(|(x, _): &(Priority, oneshot::Sender<Allocation>)| *x);
        let mut allocation_queue = VecDeque::new();

        let mut match_half_stream = self.allocation_receiver.map(MatchHalf::Supply).select(
            self.request_receiver
                .map(|(priority, request)| MatchHalf::Demand(priority, request)),
        );

        loop {
            match await!(match_half_stream.next()) {
                Some(MatchHalf::Supply(allocation)) => {
                    allocation_queue.push_back(allocation);
                }
                Some(MatchHalf::Demand(priority, request)) => {
                    request_heap.push((priority, request));
                }
                None => break,
            }

            // Do the matchy thing!
            if !request_heap.is_empty() && !allocation_queue.is_empty() {
                let (_, request) = request_heap.pop().unwrap();
                let allocation = allocation_queue.pop_front().unwrap();

                request.send(allocation).ok();
            }
        }
    }
}
