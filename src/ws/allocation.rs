use bincode::serialize;
use fish_core::message::FromServer;
use fish_core::Unit;
use futures::channel::mpsc::UnboundedReceiver;

use super::socket_event::SocketEvent;
use crate::broker::{Broker, BrokerEvent};

pub type Allocation = (AllocationSendHalf, AllocationReceiveHalf);

/// Corresponds to an opened connection to a unique client in the cloud.
#[derive(Clone)]
pub struct AllocationSendHalf {
    /// Sends messages into the WebSocket.
    ws_sender: ws::Sender,
    /// A broker to signal when the current allocation was dropped.
    broker: Broker,
}

impl AllocationSendHalf {
    pub(crate) fn new(ws_sender: ws::Sender, broker: Broker) -> AllocationSendHalf {
        AllocationSendHalf { ws_sender, broker }
    }
    /// Sends serializable data through the WebSocket connection without returning any response.
    pub(crate) fn send<U: Unit>(&self, t: &FromServer<U>) -> Result<(), ws::Error> {
        // Serializing:
        let serialized = serialize(t).unwrap(); // Should never fail.

        // Sending:
        self.ws_sender.send(ws::Message::Binary(serialized))
    }
}

impl Drop for AllocationSendHalf {
    fn drop(&mut self) {
        self.broker.send_event(BrokerEvent::Discarded);
        self.ws_sender.close(ws::CloseCode::Normal).ok(); // TODO maybe leak connection?
    }
}

pub struct AllocationReceiveHalf {
    /// Receives messages from the `MessageHandler` for this connection.
    event_stream: UnboundedReceiver<SocketEvent>,
}

impl AllocationReceiveHalf {
    pub(crate) fn new(event_stream: UnboundedReceiver<SocketEvent>) -> AllocationReceiveHalf {
        AllocationReceiveHalf { event_stream }
    }

    pub(crate) fn event_stream_mut(&mut self) -> &mut UnboundedReceiver<SocketEvent> {
        &mut self.event_stream
    }
}
