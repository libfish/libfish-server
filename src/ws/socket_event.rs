use bincode;
use fish_core::message::FromClient;
use fish_core::Unit;

#[derive(Debug)]
pub enum Error {
    Close(ws::CloseCode, String),
    Shutdown,
    DecodeError(bincode::Error),
}

/// A type for events emmited by the `MessageHandler`.
#[derive(Debug)]
pub enum SocketEvent {
    /// Received a message from the network.
    Message(ws::Message),
    /// Message handler was closed due to some reason, given in its close code.
    Close(ws::CloseCode, String),
    // /// Serializing went wrong.
    // EncodeError(bincode::Error),
    /// Web Socket is shutting down.
    Shutdown,
}

impl SocketEvent {
    pub fn decode<U: Unit>(self) -> Result<FromClient<U>, Error> {
        match self {
            SocketEvent::Message(message) => Ok(bincode::deserialize(&message.into_data())
                .map_err(|err| Error::DecodeError(err))?),
            SocketEvent::Close(close_code, message) => Err(Error::Close(close_code, message)),
            SocketEvent::Shutdown => Err(Error::Shutdown),
        }
    }
}
