use futures::channel::mpsc::UnboundedSender;
use log::warn;
use ws::{Handler, Message, Result as WsResult};

use super::socket_event::SocketEvent;

/// A structure to handle incomming messages from the WebSocket. It basically transmits the message
/// to "somewhere else" through a channel.
pub struct MessageHandler {
    /// A sender to send messages somewhere where it can be used.
    event_sender: UnboundedSender<SocketEvent>,
}

impl MessageHandler {
    pub fn new(event_sender: UnboundedSender<SocketEvent>) -> MessageHandler {
        MessageHandler { event_sender }
    }
}

impl Handler for MessageHandler {
    fn on_message(&mut self, msg: Message) -> WsResult<()> {
        // Just send to the event strem and don't panic if it is closed.
        self.event_sender
            .unbounded_send(SocketEvent::Message(msg))
            .map_err(|_| warn!("Handler failed to send message."))
            .ok();

        Ok(())
    }

    fn on_close(&mut self, close_code: ws::CloseCode, reason: &str) {
        // Tell the event stream that you are closing already.
        self.event_sender
            .unbounded_send(SocketEvent::Close(close_code, reason.to_owned()))
            .map_err(|_| warn!("Handler failed to send close event."))
            .ok();
    }

    fn on_shutdown(&mut self) {
        // Tell the event stream that whole WebSocket is closing.
        self.event_sender
            .unbounded_send(SocketEvent::Shutdown)
            .map_err(|_| warn!("Handler failed to send shutdown event."))
            .ok();
    }
}
